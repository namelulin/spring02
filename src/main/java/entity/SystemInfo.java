/**
 * @file SystemInfo.java
 * @project spel
 * @copyright 无锡雅座在线科技股份有限公司
 */
package entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * description
 *
 * 2018-04-19 15:03
 * @author lulin
 */
@Component
public class SystemInfo
{
    private String name;
    private String java;

  /*  public SystemInfo( @Value("#{systemProperties['user.name']}")String name,  @Value("#{systemProperties['user.name']}")String java) {
        this.name = name;
        this.java = java;
    }*/

    public SystemInfo() {
        System.out.println("1234");
    }

    public String getName() {
        return name;
    }

    @Value("#{systemProperties['user.name']}")
    public void setName(String name) {
        this.name = name;
    }

    public String getJava() {
        return java;
    }
    @Value("#{systemProperties['java.runtime.version']}")
    public void setJava(String java) {
        this.java = java;
    }
}
