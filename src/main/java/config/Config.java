package config;

import aop.AopTest;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import static org.junit.Assert.assertEquals;

/**
 * Created by lulin on 2018/4/18.
 */
@ComponentScan(basePackages = {"service","entity"},excludeFilters =
        {@ComponentScan.Filter(type = FilterType.ANNOTATION,value = EnableWebMvc.class)})
@Configuration
//@Import()
//@ImportResource("")
@PropertySource("classpath:properties/config.properties")
//@ImportResource("classpath:mvc/spring-mvc-config.xml")
//@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Config {
    @Bean
   public AopTest aopTest(){
       return new AopTest();
   }
}
