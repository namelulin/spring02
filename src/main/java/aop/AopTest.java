/**
 * @file AopTest.java
 * @project spel
 * @copyright 无锡雅座在线科技股份有限公司
 */
package aop;

import com.alibaba.fastjson.JSONObject;
import entity.SystemInfo;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * description
 *
 * 2018-04-20 09:54
 * @author lulin
 */
@Aspect
@Order
public class AopTest {
    @Pointcut("execution(* service..*.*(..)) || @within(org.springframework.stereotype.Controller)")
    public void action(){}

    private void printParam(JoinPoint joinPoint,HttpServletRequest request) throws NoSuchFieldException, IllegalAccessException {

        if(null==joinPoint){
            return;
        }
        Object[] param = joinPoint.getArgs();
        String params=JSONObject.toJSONString(param);
        System.out.println(params);
        for (Object object:param
                ) {
            System.out.println("入参："+JSONObject.toJSONString(object));





        }
        System.out.println("-->"+request.getLocalAddr()+":"+request.getServerPort()+request.getRequestURI());
        String method=joinPoint.getSignature().getName();
        System.out.println("路径:"+joinPoint.getTarget().getClass().getCanonicalName()+method);



      //  Modifier.toString(joinPoint.getSignature().getModifiers());
    }

   /* @Before(value = "action()")
    public void sayAction(JoinPoint joinPoint){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
       *//* request.getRemoteAddr();
        request.getRequestURI();
        request.getServerPort();
        request.getLocalPort();
        request.getServerName();
        request.getLocalAddr();
        request.getLocalName();*//*
        printParam(joinPoint,request);
    }*/

    /*@AfterReturning(returning ="result", pointcut = "execution(* service..*.*(..))")
    public Object sayEnd(JoinPoint joinPoint,Object result){
        System.out.println(result.toString());
        return result;
    }
*/
    @Around("action()")
    public Object sayEnd2(ProceedingJoinPoint point) throws Throwable{
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        printParam(point,request);
        Object name = point.proceed();
        System.out.println("12344");
        return name;
    }
}
