package controller;

import entity.SystemInfo;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.TestService;

import javax.annotation.Resource;
import java.rmi.StubNotFoundException;
import java.time.LocalDateTime;

/**
 * Created by lulin on 2018/4/18.
 */
@Controller
@RequestMapping("/test")
public class TestController implements BeanFactoryAware {

    @Autowired
    private BeanFactory beanFactory;
    @Autowired
    private Environment environment;

   // @Autowired
    @Value("#{testService}")
    private TestService testService;
    @Autowired
    private SystemInfo systemInfo;



    @Value("#{systemInfo.name}")
    private String sysjava;


    /*spel*/
    @Value("#{ systemProperties['java.version'] }")
    private String javaVersion;

    @Value("#{12}")
    private int age;

    @Value("#{T(System).currentTimeMillis()}")
    private Long time;

    @Value("#{T(Math).random()}")
    private double math;


    @Value("${name}")
    private String name;

    @RequestMapping("say")
    public String say(){
        System.currentTimeMillis();
        String address=testService.sayBaidu();
        return "redirect:"+address;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory=beanFactory;
       System.out.println(beanFactory.containsBean("testService"));
        System.out.println(beanFactory.containsBean("testServiceImpl"));

    }

    @PostMapping("system")
    @ResponseBody
    public String SystemInfo(@RequestBody SystemInfo systemInfo){
      String str=testService.sayStudent(systemInfo);
      return str;
    }

    @PostMapping("/")
    @ResponseBody
    public String test(@RequestBody SystemInfo systemInfo){
        String str=testService.sayStudent(systemInfo);
        return str;
    }

    @RequestMapping(value = "home")
    public String testHome(){
        String str=testService.sayStudent(systemInfo);
        return "home";
    }
}
