/**
 * @file MvcConfig.java
 * @project spring03
 * @copyright 无锡雅座在线科技股份有限公司
 */
package mvcconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * description
 *
 * 2018-04-24 15:24
 * @author lulin
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"controller"})
public class MvcConfig extends WebMvcConfigurerAdapter {
    @Bean
    public ViewResolver viewResolver(){
        InternalResourceViewResolver viewResolver=new InternalResourceViewResolver();
        viewResolver.setExposeContextBeansAsAttributes(true);
        viewResolver.setSuffix(".jsp");
        viewResolver.setPrefix("/WEB-INF/views/");
        return viewResolver;
    }
    /*静态资源处理*/
    @Override
    public void  configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer){
       configurer.enable();
    }

}
