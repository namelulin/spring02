/**
 * @file MvcConfig.java
 * @project spring03
 * @copyright 无锡雅座在线科技股份有限公司
 */
package mvcconfig;

import config.Config;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * description
 *
 * 2018-04-24 15:16
 * @author lulin
 */
public class Split extends AbstractAnnotationConfigDispatcherServletInitializer {

    /*返回的带有@Configuration注解的类将会用来配置ContextLoaderListener创建的应用上下文中的bean*/
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{Config.class};
    }
 /*返回的带有@Configuration注解的类将会用来定
义DispatcherServlet应用上下文中的
bean*/
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{MvcConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
