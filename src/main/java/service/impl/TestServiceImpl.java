package service.impl;

import com.sun.corba.se.impl.interceptors.PICurrent;
import entity.SystemInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import service.TestService;

import static java.lang.Math.PI;
import static javafx.scene.input.KeyCode.T;

/**
 * Created by lulin on 2018/4/18.
 */
@Service("testService")
public class TestServiceImpl implements TestService {


    @Override
    public String sayBaidu() {
        return "https://www.baidu.com/";
    }

    @Override
    public String sayStudent(SystemInfo systemInfo) {
        System.out.println("123454");
        if(null==systemInfo){
            System.out.println("参数为空");
        }
        return "lalalalalalal";
    }
}
