package service;

import entity.SystemInfo;

/**
 * Created by lulin on 2018/4/18.
 */
public interface TestService {
    String sayBaidu();
    String sayStudent(SystemInfo systemInfo);
}
